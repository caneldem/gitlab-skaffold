.PHONY: bootstrap
bootstrap: docker/.git charts/.git gitlab/.git gitlab-workhorse/.git gitlab/configure docker/rails

docker/.git:
	git clone https://gitlab.com/aevstifeev/gitlab-docker.git

docker/rails:
	docker build -t gitlab-skaffold/gitlab-rails -f Dockerfile.rails .

charts/.git:
	git clone https://gitlab.com/aevstifeev/gitlab-charts.git

gitlab/.git:
	git clone https://gitlab.com/gitlab-org/gitlab.git

gitlab/configure:
	cd gitlab && \
	cp config/gitlab.yml.example config/gitlab.yml && \
	cp config/resque.yml.example config/resque.yml && \
	cp config/secrets.yml.example config/secrets.yml && \
	cp config/database.yml.postgresql config/database.yml && \
	cp config/initializers/rack_attack.rb.example config/rack_attack.rb && \
	sed --in-place "/host: localhost/d" config/gitlab.yml && \
	sed --in-place "/port: 80/d" config/gitlab.yml && \
	sed --in-place "s/# user:.*/user: ${GITLAB_USER}/" config/gitlab.yml && \
	sed --in-place "s:/home/git/repositories:${DATADIR}/repo:" config/gitlab.yml

gitlab-workhorse/.git:
	git clone https://gitlab.com/gitlab-org/gitlab-workhorse.git
